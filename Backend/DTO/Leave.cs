﻿using System;

namespace DTO
{
    public class Leave
    {
        public Leave()
        {
            
        }

        public int Id { get; set; }
        public int EmployeeId { get; set; }
        public int LeaveTypeId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime RequestedDate { get; set; }
        public DateTime ApprovedDate { get; set; }
        public int ApprovedByEmployeeId { get; set; }
        public string Remark { get; set; }    
    }

    public class LeaveRequest
    {
        public LeaveRequest()
        {

        }

        public int Id { get; set; }
        public int EmployeeId { get; set; }
        public int LeaveTypeId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime RequestedDate { get; set; }
    }
}
