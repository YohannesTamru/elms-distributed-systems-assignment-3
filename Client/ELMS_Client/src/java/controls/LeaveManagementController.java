/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controls;

import boundary.EmployeeManagementBoundary;
import boundary.LeaveRequestManagementBoundary;
import com.sun.org.apache.xerces.internal.jaxp.datatype.XMLGregorianCalendarImpl;
import elms.soa.services.rmi.Leave;
import elms.soa.services.rpc.EmployeeLeaveInfo;
import elms.soa.services.rpc.LeaveType;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.xml.bind.JAXBElement;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import model.LeaveRequestModel;


/**
 *
 * @author Mike GTE
 */
@Named(value = "leaveManagementController")
@ViewScoped
public class LeaveManagementController implements Serializable {

    /**
     * Creates a new instance of LeaveManagementController
     */
    public LeaveManagementController() {
        
    }
    
    @EJB
    private EmployeeManagementBoundary employeeManagementBoundary;
    @EJB
    private LeaveRequestManagementBoundary leaveRequestManagementBoundary;

    List<EmployeeLeaveInfo> employeeLeaveStatus = new ArrayList<>();
    String empName;
    LeaveRequestModel leaveRequest = new LeaveRequestModel();
    
    List<LeaveType> leaveTypes = new ArrayList<>();
    LeaveType addLeaveType = new LeaveType();

    
    public LeaveRequestModel getLeaveRequest() {    
        return leaveRequest;
    }

    public LeaveType getAddLeaveType() {
        return addLeaveType;
    }

//<editor-fold defaultstate="collapsed" desc="Getter and Setters">
    public void setAddLeaveType(LeaveType addLeaveType) {
        this.addLeaveType = addLeaveType;
    }

    public List<LeaveType> getLeaveTypes() {
        return GetAllLeaveTypes();
    }

    public void setLeaveTypes(List<LeaveType> leaveTypes) {
        this.leaveTypes = leaveTypes;
    }
    
    
    public void setLeaveRequest(LeaveRequestModel leaveRequest) {
        this.leaveRequest = leaveRequest;
    }

    public String getEmpName() {
        return empName;
    }

    public void setEmpName(String empName) {
        this.empName = empName;
    }

    public List<EmployeeLeaveInfo> getEmployeeLeaveStatus() {
        if(empName == null || empName.isEmpty())
            return employeeManagementBoundary.getAllEmployeeLeaveInfo();
        else
            return employeeLeaveStatus;
    }

    public void setEmployeeLeaveStatus(List<EmployeeLeaveInfo> employeeLeaveStatus) {
        this.employeeLeaveStatus = employeeLeaveStatus;
    }

//</editor-fold>
    
    
    public void searchEmployeeLeaveInfo() {
        
        if (empName == null) {
            empName = "";
        }
        resetEmployeeLeaveStatus();
        employeeLeaveStatus = employeeManagementBoundary.searchEmployeeLeaveInfo(empName);
        
    }

    
    public void submitRequest(){
        
        if(leaveRequest == null)
            return;
        
        Leave leave = new Leave();
        GregorianCalendar fromDate = new GregorianCalendar();
        fromDate.setTime(leaveRequest.getFromDate());
        
        XMLGregorianCalendar fd = null;
        
        
        GregorianCalendar toDate = new GregorianCalendar();
        fromDate.setTime(leaveRequest.getToDate());
        
        XMLGregorianCalendar td = null;
        
        try {
            fd = DatatypeFactory.newInstance().newXMLGregorianCalendar(fromDate);
            td = DatatypeFactory.newInstance().newXMLGregorianCalendar(toDate);
        } catch (DatatypeConfigurationException ex) {
            
            Logger.getLogger(LeaveManagementController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        leave.setStartDate(fd);
        leave.setEndDate(td);
        leave.setEmployeeId(leaveRequest.getEmpId());
//        leave.setRemark(leaveRequest.getReason());
        
        leaveRequestManagementBoundary.requestLeave(leave);
        
        resetLeaveRequest();
        
    }
    
    public List<LeaveType> GetAllLeaveTypes(){
        
        return employeeManagementBoundary.getAllLeaveTypes();
    }   
    
    public void removeLeaveType(int leaveTypeId){
        System.out.println("leaveTypeId : " + leaveTypeId);
    }
    
    public void addNewLeaveType(){
        if(addLeaveType == null)
            return;
        employeeManagementBoundary.addNewLeaveType(addLeaveType);
    }
    
    
    private void resetEmployeeLeaveStatus(){
        employeeLeaveStatus = new ArrayList<>();
    }
    
    private void resetLeaveRequest(){
        leaveRequest = new LeaveRequestModel();
    }
    
    
}
