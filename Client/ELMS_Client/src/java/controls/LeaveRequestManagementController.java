/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controls;

import boundary.LeaveRequestManagementBoundary;
import elms.soa.services.rmi.EmployeeLeaveInfo;
import elms.soa.services.rmi.Leave;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.faces.view.ViewScoped;

/**
 *
 * @author Mike GTE
 */
@Named(value = "leaveRequestManagementController")
@ViewScoped
public class LeaveRequestManagementController  implements Serializable {

    
    @EJB
    private LeaveRequestManagementBoundary leaveRequestManagementBoundary;
    
    List<EmployeeLeaveInfo> leaveRequest = new ArrayList<>();

    public List<EmployeeLeaveInfo> getLeaveRequest() {
        return leaveRequest;
    }

    public void setLeaveRequest(List<EmployeeLeaveInfo> leaveRequest) {
        this.leaveRequest = leaveRequest;
    }
    
    public LeaveRequestManagementController() {
    }
    
    public void ApproveRequest(int rqstId){
        leaveRequestManagementBoundary.ApproveRequest(rqstId);
    }
    
    public void GetAllLeaveRequests(){
        leaveRequestManagementBoundary.GetAllLeaveRequests();
    }
    
    
    
    
}
