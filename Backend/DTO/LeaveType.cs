﻿
namespace DTO
{
    public class LeaveType
    {
        public LeaveType()
        {
            
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
