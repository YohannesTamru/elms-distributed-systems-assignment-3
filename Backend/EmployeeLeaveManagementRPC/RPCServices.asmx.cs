﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web.Services;
using DataAccess;
using DTO;
using Employee = DataAccess.Employee;
using Organization = DataAccess.Organization;

namespace EmployeeLeaveManagementRPC
{
    /// <summary>
    /// Summary description for RPCServices
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    //[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class RpcServices : WebService
    {
        readonly LeaveManagementSystemEntities _dataContext = new LeaveManagementSystemEntities();

        [WebMethod]
        public DTO.Employee[] GetEmployees()
        {
            return _dataContext.Employees.Select(t => new DTO.Employee
            {
                FirstName = t.FirstName,
                LastName = t.LastName,
                OrganizationId = t.Organization.Id,
                PhoneNumber = t.PhoneNumber,
                Id = t.Id
            }).ToArray();
        }

        [WebMethod]
        public DTO.LeaveType[] GetLeaveTypes()
        {
            return _dataContext.LeaveTypes.Select(t => new DTO.LeaveType()
            {
                Id = t.Id,
                Name = t.Name,
                Description = t.Description
            }).ToArray();
        }

        [WebMethod]
        public void AddNewLeaveType(DTO.LeaveType lt)
        {
            var leaveType = new DataAccess.LeaveType
            {
                Name = lt.Name,
                Description = lt.Description
            };

            _dataContext.AddToLeaveTypes(leaveType);
            _dataContext.SaveChanges();
        }


        //[WebMethod]
        //public IEnumerable<DTO.Organization> GetOrganizations()
        //{
        //    return _dataContext.Organizations.Select(t => new DTO.Organization()
        //    {
        //        Id = t.Id,
        //        Name = t.Name
        //    });
        //}



        [WebMethod]
        public DTO.Organization[] GetOrganizations()
        {
            return _dataContext.Organizations.Select(t => new DTO.Organization()
            {
                Id = t.Id,
                Name = t.Name,
                IsActive = t.IsActive
            }).ToArray();
        }


        [WebMethod]
        public void SaveEmployeeInformation(DTO.Employee emplInfo)
        {
            
            var empEntity = new Employee
            {
                FirstName = emplInfo.FirstName,
                LastName = emplInfo.LastName,
                PhoneNumber = emplInfo.PhoneNumber,
                Organization = _dataContext.Organizations.FirstOrDefault(org => org.Id == emplInfo.OrganizationId)
            };

            _dataContext.AddToEmployees(empEntity);

            _dataContext.SaveChanges();
            
        }

        [WebMethod]
        public EmployeeLeaveInfo[] SearchEmployeeLeaveStatus(string empName)
        {

            var query = from emp in _dataContext.View_EmployeeLeaveStatus
                        select emp;

            if (!string.IsNullOrEmpty(empName))
                query = query.Where(emp => emp.FullName.ToLower().Contains(empName));


            return query.Select(e => new EmployeeLeaveInfo
            {
                Id = e.ID,
                FullName = e.FullName,
                LeaveStatus = e.LeaveStatus,
                LeaveType = (e.LeaveStatus != "None") ? e.LeaveType : "NA",
                Organization = e.OrganizationName
            }).ToArray();

        }

    }
}
