﻿namespace DTO
{
    public class EmployeeLeaveInfo
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public string Organization { get; set; }
        public string LeaveStatus { get; set; }
        public string LeaveType { get; set; }
    }
}
