﻿namespace DTO
{
    public class Organization
    {
        public Organization()
        {
            
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
    }
}
