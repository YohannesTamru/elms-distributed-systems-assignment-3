/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package boundary;

import elms.soa.services.rmi.*;
import java.util.List;
import javax.ejb.Stateless;
import model.LeaveRequestModel;

/**
 *
 * @author Mike GTE
 */
@Stateless
public class LeaveRequestManagementBoundary {

    
    RMIService _rmiService;
    IRMIService _rmiPort;

    public LeaveRequestManagementBoundary() {
        
        _rmiService = new elms.soa.services.rmi.RMIService();
        _rmiPort = _rmiService.getBasicHttpBindingIRMIService();

    }

    public List<EmployeeLeaveInfo> GetAllLeaveRequests(){
        return _rmiPort.getAllLeaveRequests("").getEmployeeLeaveInfo();
    }
    
    public void ApproveRequest(int requestId){
        System.out.println("rId : " + requestId);
//        _rmiPort.(requestId);
    }
    
    
    public void requestLeave(Leave leave){
        _rmiPort.requestLeave(leave);
    }
}
