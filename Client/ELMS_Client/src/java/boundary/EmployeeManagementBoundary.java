/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package boundary;

import javax.ejb.Stateless;
import model.EmployeeInfoModel;
//import elms.soa.services.rmi.*;
import elms.soa.services.rpc.*;
import java.util.List;

/**
 *
 * @author Mike GTE
 */
@Stateless
public class EmployeeManagementBoundary {

//    RMIService _rmiService;
//    IRMIService _rmPort;
    RpcServices _rpcService;
    RpcServicesSoap _rpcPort;

    public EmployeeManagementBoundary() {

//        _rmiService = new elms.soa.services.rmi.RMIService();
//        _rmPort = _rmiService.getBasicHttpBindingIRMIService();

        _rpcService = new elms.soa.services.rpc.RpcServices();
        _rpcPort = _rpcService.getRpcServicesSoap();
    }

    public void RegisterEmployee(EmployeeInfoModel employeeInfo) {
        
        Employee empInfoDto = new Employee();
        empInfoDto.setFirstName(employeeInfo.getFirstName());
        empInfoDto.setLastName(employeeInfo.getLastName());
        empInfoDto.setPhoneNumber(employeeInfo.getPhoneNumber());
        empInfoDto.setOrganizationId(1);

        _rpcPort.saveEmployeeInformation(empInfoDto);
        
    }
    
    
    public List<EmployeeLeaveInfo> searchEmployeeLeaveInfo(String empName){
        
        ArrayOfEmployeeLeaveInfo result = _rpcPort.searchEmployeeLeaveStatus(empName);
        
        return result.getEmployeeLeaveInfo();
    }

    
    
    public List<EmployeeLeaveInfo> getAllEmployeeLeaveInfo(){
        
        return searchEmployeeLeaveInfo("");
        
    }
    
    public List<LeaveType> getAllLeaveTypes(){
        return _rpcPort.getLeaveTypes().getLeaveType();
    }
    
    public void addNewLeaveType(LeaveType leaveType){
        _rpcPort.addNewLeaveType(leaveType);
    }

    
}
