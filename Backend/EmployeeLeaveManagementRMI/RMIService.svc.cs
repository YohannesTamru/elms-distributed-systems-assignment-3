﻿using System.Collections.Generic;
using System.Linq;
using DataAccess;
using DTO;
using Employee = DataAccess.Employee;
using LeaveType = DataAccess.LeaveType;

namespace EmployeeLeaveManagementRMI
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class RMIService : IRMIService
    {
        private readonly LeaveManagementSystemEntities _dataContext = new LeaveManagementSystemEntities();

        public IEnumerable<DTO.Leave> GetLeaves()
        {
            return _dataContext.Leaves.Select(t => new DTO.Leave
            {
                ApprovedByEmployeeId = t.ApprovedByEmployeeId,
                ApprovedDate = t.ApprovedDate,
                EmployeeId = t.Employee.Id,
                EndDate = t.EndDate,
                LeaveTypeId = t.LeaveType.Id,
                Remark = t.Remark,
                StartDate = t.StartDate,
                RequestedDate = t.RequestedDate,
                Id = t.Id
            }).ToList();
        }

        public void Save(DTO.Leave t)
        {
            var leaveObj = new DataAccess.Leave()
            {
                ApprovedByEmployeeId = t.ApprovedByEmployeeId,
                ApprovedDate = t.ApprovedDate,  
                Employee = new Employee() { Id = t.EmployeeId },
                EndDate = t.EndDate,
                LeaveType = new LeaveType { Id =  t.LeaveTypeId },
                Remark = t.Remark,
                StartDate = t.StartDate,
                RequestedDate = t.RequestedDate,
                Id = t.Id
            };

            _dataContext.AddToLeaves(leaveObj);

            _dataContext.SaveChanges();
        }



        public void RequestLeave(DTO.Leave t)
        {
            var leaveObj = new DataAccess.Leave()
            {
                Employee = new Employee() { Id = t.EmployeeId },
                EndDate = t.EndDate,
                Remark = t.Remark,
                StartDate = t.StartDate,
                RequestedDate = t.RequestedDate
            };

            _dataContext.AddToLeaves(leaveObj);

            _dataContext.SaveChanges();
        }



        
        public EmployeeLeaveInfo[] GetAllLeaveRequests(string empName)
        {

            var query = from emp in _dataContext.View_EmployeeLeaveStatus
                        select emp;

            if (!string.IsNullOrEmpty(empName))
                query = query.Where(emp => emp.FullName.ToLower().Contains(empName));

            query = query.Where(emp => emp.LeaveStatus == "Requested");

            return query.Select(e => new EmployeeLeaveInfo
            {
                Id = e.ID,
                FullName = e.FullName,
                LeaveStatus = e.LeaveStatus,
                LeaveType = (e.LeaveStatus != "None") ? e.LeaveType : "NA",
                Organization = e.OrganizationName
            }).ToArray();

        }


    }
}
