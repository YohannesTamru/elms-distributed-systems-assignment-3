﻿namespace DTO
{
    public class Employee
    {
        public Employee()
        {
            
        }

        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }

        public int OrganizationId { get; set; }

    }
}
