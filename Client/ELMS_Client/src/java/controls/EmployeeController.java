/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controls;

import boundary.EmployeeManagementBoundary;
import elms.soa.services.rmi.Leave;
import java.io.Serializable;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import model.EmployeeInfoModel;

/**
 *
 * @author Mike GTE
 */
@Named(value = "employee")
@ViewScoped
public class EmployeeController implements Serializable {

    @EJB
    private EmployeeManagementBoundary employeeManagementBoundary;

    EmployeeInfoModel employeeInfo = new EmployeeInfoModel();
//<editor-fold defaultstate="collapsed" desc="Getter and Setters">

    public EmployeeInfoModel getEmployeeInfo() {
        if (employeeInfo == null) {
            return new EmployeeInfoModel();
        } else {
            return employeeInfo;
        }
    }

    public void setEmployeeInfo(EmployeeInfoModel employeeInfo) {
        this.employeeInfo = employeeInfo;
    }

//</editor-fold>
    
    public EmployeeController() {
          
    }
    
    public void registerEmployee() {

        if (employeeInfo == null) {
            //Raise validation exception
            return;
        }

        employeeManagementBoundary.RegisterEmployee(employeeInfo);
        
        resetRegistrationForm();
    }
    
    
    public void requestEmployee(Leave leave){
        
        
        
    }
    
    private void resetRegistrationForm(){
        employeeInfo = new EmployeeInfoModel();
    }
    
    
}
