﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ServiceModel;
using DTO;

namespace EmployeeLeaveManagementRMI
{
    [ServiceContract]
    public interface IRMIService
    {

        [OperationContract]
        IEnumerable<Leave> GetLeaves();

        [OperationContract]
        void Save(Leave leave);

        [OperationContract]
        void RequestLeave(DTO.Leave t);

        [OperationContract]
        EmployeeLeaveInfo[] GetAllLeaveRequests(string empName);

    }


}
