CREATE VIEW LMS.View_EmployeeLeaveStatus
AS
SELECT
EMP.ID,
(EMP.FirstName + ' ' + EMP.LastName) AS FullName,
ORG.Name AS OrganizationName,
CASE WHEN (L.StartDate <= GETDATE() AND L.EndDate >= GETDATE() AND L.ApprovedDate IS NOT NULL) THEN 'On Leave'
	 WHEN (L.RequestedDate IS NOT NULL AND L.ApprovedDate IS NULL) THEN 'Requested'
	 ELSE 'None' END AS LeaveStatus,
LT.Description AS LeaveType

FROM LMS.Employee AS EMP
LEFT JOIN LMS.Organization AS ORG ON EMP.OrganizationId = ORG.Id
LEFT JOIN LMS.Leave AS L ON L.EmployeeId = EMP.Id
LEFT JOIN LMS.LeaveType AS LT ON L.LeaveTypeId = LT.Id